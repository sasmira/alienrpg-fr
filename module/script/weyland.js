Hooks.once('init', () => {
    game.settings.register('alienrpg-fr', 'flickering', {
        name: game.i18n.localize('ALIENRPG-FR.flickering'),
        hint: game.i18n.localize('ALIENRPG-FR.flickeringHint'),
        scope: 'client',
        type: Boolean,
        default: false,
        config: true,
        onChange: () => {
            location.reload();
        },
    });
    game.settings.register('alienrpg-fr', 'screenDoor', {
        name: game.i18n.localize('ALIENRPG-FR.screenDoor'),
        hint: game.i18n.localize('ALIENRPG-FR.screenDoorHint'),
        scope: 'client',
        type: Boolean,
        default: false,
        config: true,
        onChange: () => {
            location.reload();
        },
    });
    game.settings.register('alienrpg-fr', 'scanline', {
        name: game.i18n.localize('ALIENRPG-FR.scanline'),
        hint: game.i18n.localize('ALIENRPG-FR.scanlineHint'),
        scope: 'client',
        type: Boolean,
        default: false,
        config: true,
        onChange: () => {
            location.reload();
        },
    });
});
Hooks.once('ready', function () {
    if (game.settings.get('alienrpg-fr', 'scanline')) {
        const scanline = $('<div>').addClass('scanline');
        $('body').append(scanline);
    }
    if (game.settings.get('alienrpg-fr', 'flickering')) {
        $('body').addClass('flickering');
    }
    if (game.settings.get('alienrpg-fr', 'screenDoor')) {
        $('body').addClass('screen-door');
    }
});
