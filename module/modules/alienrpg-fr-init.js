/************************************************************************************/
Hooks.once('init', () => {

	if(typeof Babele !== 'undefined') {
		  
	  console.log("BABELE LOADED !!!");
		  Babele.get().register({
			  module: 'alienrpg-fr',
			  lang: 'fr',
			  dir: 'compendiums'
		  });
	}
  });
  
  Hooks.once('ready', () => {
  game.items.getName("Close Combat").data.data.description = `<p>Pour chaque succ&egrave;s obtenu en plus du premier, choisissez une action d'&eacute;clat coh&eacute;rente avec le contexte:</p>
  <ol>
  <li>Vous infligez 1 point de d&eacute;g&acirc;ts de plus. Vous pouvez choisir cette action d'&eacute;clat plusieurs fois si vous avez obtenu plus de deux succ&egrave;s.</li>
  <li>En feintant l'ennemi, vous &eacute;changez vos initiatives (cf. page 53) &agrave; partir du Round suivant. Vous ne pouvez pas reprendre votre initiative initiale.</li>
  <li>Vous faites l&acirc;cher un objet ou une arme &agrave; votre adversaire, ou vous le/la lui arrachez (&agrave; vous de choisir). Pendant un combat, ramasser un objet tomb&eacute; constitue une action rapide (cf. page 53). Votre adversaire se retrouve &agrave; terre.</li>
  <li>Vous saisissez votre adversaire, qui doit d&eacute;sormais remporter un test oppos&eacute; de COMBAT RAPPROCH&Eacute; pour se lib&eacute;rer de votre prise et ne peut effectuer aucune action tant qu'il n'y est pas parvenu, que vous n'&ecirc;tes pas Bris&eacute; ou que vous ne l'avez pas l&acirc;ch&eacute;. Cet effet ne fonctionne que sur les adversaires humano&iuml;des.</li>
  </ol>`;
  
  game.items.getName("Command").data.data.description = `<p style="font-size: 14px;">Si vous voulez survivre aux horreurs de l'espace, vous avez besoin d'un bon leader... ou vous devez en devenir un vous-m&ecirc;me. Vous pouvez utiliser votre comp&eacute;tence COMMANDEMENT de deux fa&ccedil;ons :</p>
  <h3>ARR&Ecirc;TER LA PANIQUE</h3>
  <p style="font-size: 14px;">Quand un autre personnage perd les p&eacute;dales apr&egrave;s un test de Panique rat&eacute;, vous pouvez effectuer un test de COMMANDEMENT pour le ramener &agrave; la raison. Reportez-vous &agrave; la page 70.</p>
  <h3>DONNER DES ORDRES</h3>
  <p style="font-size: 14px;">Au combat, au prix d'une action lente, vous pouvez aboyer un ordre &agrave; destination d'un autre personnage. Celui-ci doit pouvoir vous entendre, ne serait-ce que par radio. Faites un test de COMMANDEMENT. Pour chaque succ&egrave;s obtenu, le personnage b&eacute;n&eacute;ficie d'un modificateur de +1 &agrave; son test lorsqu'il ex&eacute;cute l'ordre en question.</p>
  <h3>OFFICIERS</h3>
  <p style="font-size: 14px;">Les PJ dot&eacute;s de la carri&egrave;re Officier et du talent Jouer du galon peuvent utiliser commandement pour ordonner &agrave; d'autres personnages (PJ et PNJ) de leur ob&eacute;ir.&nbsp;</p>
  <h3>CE N'EST PAS DE LA DOMINATION MENTALE !</h3>
  <p style="font-size: 14px;">Quand vous utilisez MANIPULATION contre quelqu&rsquo;un, vous ne prenez pas le contr&ocirc;le de son esprit. Vous ne pouvez tenter de le convaincre que de quelque chose de raisonnable, sinon Maman ne vous le permettra pas.</p>`;
  
  game.items.getName("Comtech").data.data.description = `<p>Pour chaque succ&egrave;s obtenu en plus du premier, choisissez une action d'&eacute;clat coh&eacute;rente avec le contexte:</p>
  <ol>
  <li>Obtenez un modificateur de +1 &agrave; un test de comp&eacute;tence ult&eacute;rieur li&eacute; &agrave; celui-ci.</li>
  <li>Plus besoin de faire de test lorsque vous &ecirc;tes confront&eacute; &agrave; une situation exactement semblable.</li>
  <li>Vous avez r&eacute;ussi en un rien de temps, deux fois plus vite que pr&eacute;vu.</li>
  <li>Vous obtenez des informations in&eacute;dites ou inattendues (choisies par Maman).</li>
  <li>Vous couvrez vos traces.</li>
  <li>Vous avez de quoi frimer.</li>
  </ol>`;
  
  game.items.getName("Heavy Machinery").data.data.description = `<p>Pour chaque succ&egrave;s obtenu en plus du premier, choisissez une action d'&eacute;clat coh&eacute;rente avec le contexte:</p>
  <ol>
  <li>Obtenez un modificateur de +1 &agrave; un test de comp&eacute;tence ult&eacute;rieur li&eacute; &agrave; celui-ci.</li>
  <li>Vous avez pig&eacute;. Plus besoin de faire de test lorsque vous &ecirc;tes confront&eacute; &agrave; une situation exactement semblable.</li>
  <li>Vous avez r&eacute;ussi en un rien de temps, deux fois plus vite que pr&eacute;vu.</li>
  <li>Vous avez mis ce truc H.S. D&eacute;finitivement.</li>
  <li>Vous avez agi en toute discr&eacute;tion.</li>
  <li>Vous avez de quoi frimer.</li>
  </ol>`;
  
  game.items.getName("Manipulation").data.data.description = `<p>Pour chaque succ&egrave;s obtenu en plus du premier, choisissez une action d'&eacute;clat coh&eacute;rente avec le contexte:</p>
  <ol>
  <li>Votre adversaire se plie &agrave; vos exigences sans rien demander en retour.</li>
  <li>Votre adversaire vous donne davantage que ce que vous lui demandiez, en vous transmettant par exemple une information utile. &Agrave; Maman de fixer les d&eacute;tails.</li>
  <li>Vous impressionnez votre adversaire, qui tentera de vous aider ult&eacute;rieurement. &Agrave; Maman de fixer les d&eacute;tails.</li>
  </ol>`;
  
  game.items.getName("Medical Aid").data.data.description = `<p>Dans le monde d&rsquo;ALIEN, vous et les autres personnages courez le risque bien r&eacute;el d'&ecirc;tre bless&eacute;s t&ocirc;t ou tard. C'est l&agrave; qu'intervient la comp&eacute;tence SOINS M&Eacute;DICAUX. Vous pouvez l'employer de deux fa&ccedil;ons:</p><h3>R&Eacute;CUP&Eacute;RATION</h3>
  <p>Un personnage dont la Sant&eacute; tombe &agrave; z&eacute;ro est Bris&eacute;. Si vous lui prodiguez aussit&ocirc;t des soins m&eacute;dicaux et r&eacute;ussissez votre test, il se remet sur pied et r&eacute;cup&egrave;re imm&eacute;diatement un nombre de points de Sant&eacute; &eacute;gal au nombre de succ&egrave;s obtenus. Renseignez-vous en consultant le chapitre 4.</p>
  <h3>SAUVER UNE VIE</h3>
  <p>L&rsquo;utilisation la plus cruciale des SOINS M&Eacute;DICAUX intervient pour sauver la vie d'un personnage victime d'une blessure critique. Dans ce contexte, un &eacute;chec peut lui co&ucirc;ter la vie, alors faites attention ! Renseignez-vous sur les blessures critiques page 65.</p>`;
  
  game.items.getName("Mobility").data.data.description = `<p>Pour chaque succ&egrave;s obtenu en plus du premier, choisissez une action d'&eacute;clat coh&eacute;rente avec le contexte:</p>
  <ol>
  <li>Donnez un succ&egrave;s &agrave; un autre PJ qui se trouve dans la m&ecirc;me situation que vous.</li>
  <li>Obtenez un modificateur de +1 &agrave; un test de comp&eacute;tence ult&eacute;rieur li&eacute; &agrave; celui-ci.</li>
  <li>Vous impressionnez quelqu&rsquo;un.</li>
  </ol>`;
  
  game.items.getName("Observation").data.data.description = `<p>Pour chaque succ&egrave;s obtenu en plus du premier, choisissez une action d'&eacute;clat coh&eacute;rente avec le contexte:</p>
  <ol>
  <li>Est-ce que &ccedil;a m'a pris pour cible ?</li>
  <li>Y'en a-t-il d'autres &agrave; proximit&eacute; ?</li>
  <li>Comment puis-je entrer/passer sans que &ccedil;a me voie/m'&eacute;chapper ?</li>
  </ol>`;
  
  game.items.getName("Piloting").data.data.description = `<p>Pour chaque succ&egrave;s obtenu en plus du premier, choisissez une action d'&eacute;clat coh&eacute;rente avec le contexte:</p>
  <ol>
  <li>Obtenez un modificateur de +1 &agrave; un test de comp&eacute;tence ult&eacute;rieur li&eacute; &agrave; celui-ci.</li>
  <li>Vous avez de quoi frimer.</li>
  </ol>`;
  
  game.items.getName("Ranged Combat").data.data.description = `<p>Pour chaque succ&egrave;s obtenu en plus du premier, choisissez une action d'&eacute;clat coh&eacute;rente avec le contexte:</p>
  <ol>
  <li>Vous infligez 1 point de d&eacute;g&acirc;ts de plus. Vous pouvez choisir cette action d'&eacute;clat plusieurs fois si vous avez obtenu plus de deux succ&egrave;s</li>
  <li>Votre ennemi est clou&eacute; sur place par vos tirs. Il doit effectuer imm&eacute;diatement un test de Panique.</li>
  <li>En changeant de position, vous &eacute;changez votre initiative avec celle de l&rsquo;adversaire (cf. page 53), qui prendra effet au prochain Round. Vous ne pouvez pas retourner &agrave; votre initiative d'origine.</li>
  <li>Votre cible l&acirc;che une arme ou un autre objet qu'elle tenait. &Agrave; vous de choisir lequel.</li>
  <li>Votre adversaire s'&eacute;croule ou recule, par exemple dans un sas...</li>
  </ol>`;
  
  game.items.getName("Stamina").data.data.description = `<p>Pour chaque succ&egrave;s obtenu en plus du premier, choisissez une action d'&eacute;clat coh&eacute;rente avec le contexte:</p>
  <ol>
  <li>Donnez un succ&egrave;s &agrave; un autre PJ qui se trouve dans la m&ecirc;me situation que vous.</li>
  <li>Obtenez un modificateur de +1 &agrave; un test de comp&eacute;tence ult&eacute;rieur li&eacute; &agrave; celui-ci.</li>
  <li>L'exp&eacute;rience vous a endurci. Plus besoin de faire de test lorsque vous &ecirc;tes confront&eacute; &agrave; une situation exactement semblable.</li>
  <li>Vous impressionnez quelqu&rsquo;un.</li>
  </ol>`;
  
  game.items.getName("Survival").data.data.description = `<p>Pour chaque succ&egrave;s obtenu en plus du premier, choisissez une action d'&eacute;clat coh&eacute;rente avec le contexte:</p>
  <ol>
  <li>Donnez un Succ&egrave;s &agrave; un autre PJ qui se trouve dans la m&ecirc;me situation que vous.</li>
  <li>Obtenez un modificateur de +1 &agrave; un test de comp&eacute;tence ult&eacute;rieur li&eacute; &agrave; celui-ci.</li>
  <li>Vous impressionnez quelqu&rsquo;un.</li>
  </ol>`;
  });