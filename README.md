#DISCLAIMER. Alien RPG version *NON-OFFICIELLE* française par [ARKHANE ASYLUM PUBLISHING](https://arkhane-asylum.fr)

<div align="center">![banniere](./banniere.jpg)</div>

Module pour système de jeu Alien RPG pour [Foundry Virtual Tabletop](https://foundryvtt.com/) qui fournit une traduction de la fiche de personnage et des systèmes de base pour jouer à Alien RPG de [ARKHANE ASYLUM PUBLISHING](https://arkhane-asylum.fr).
- Cette version est une version *NON-OFFICIELLE* française.
- Le livre de règles en français, disponible chez [ARKHANE ASYLUM PUBLISHING](https://arkhane-asylum.fr), est nécessaire afin de pouvoir exploiter pleinement le module en français.

## Installer avec le manifeste
Copier ce lien et chargez-le dans le menu module de Foundry.
> https://gitlab.com/sasmira/alienrpg-fr/-/raw/master/module/module.json

### Modules requis pour le français
Pour traduire les éléments de base de FoundryVTT (interface), il vous faut installer et activer le module suivant :
> https://gitlab.com/baktov.sugar/foundryvtt-lang-fr-fr

La traduction du système fonctionne directement, cependant les compendiums nécessitent d'installer et activer le module Babele pour être traduit :
> https://gitlab.com/riccisi/foundryvtt-babele

### Modules recommandés
- Dice so Nice, pour avoir des dés 3D : https://gitlab.com/riccisi/foundryvtt-dice-so-nice
- Search Anywhere : https://gitlab.com/riccisi/foundryvtt-search-anywhere (pour ne pas perdre top de temps à chercher une technique)


## Nous rejoindre
1. Vous pouvez retrouver toutes les mises à jour en français pour FoundryVTT sur le discord officiel francophone
2. Lien vers [Discord Francophone](https://discord.gg/pPSDNJk)

## L'équipe d'Alien actuelle des contributeurs (par ordre alphabétique)
- Carter
- Lightbringer
- Sasmira

## Remerciements, vielen danke & Many thanks to :
1. Sasmira et LeRatierBretonnien pour leur travail sur la [communauté Francophone de Foundry](https://discord.gg/pPSDNJk)
2. Carter pour leur travail sur la traduction de la feuille de personnage et ses aides de jeux
3. Lightbringer, pour les sons et corrections diverses.

## Contribuer
Vous êtes libre de contribuer et proposer après fork des corrections, modifications. Essayez de respecter 3 règles :
1. Assurez-vous de bien être à jour par rapport à la branche référente.
2. Des messages de commit clair et précis permettent une relecture rapide du code.
3. Limitez-vous si possible à une Feature par demande de Merge pour ne pas bloquer le processus.


