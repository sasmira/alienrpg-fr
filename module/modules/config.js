export const SidebarAestheticsConfig = {};

SidebarAestheticsConfig.chatPortraitSizes = {
  "smallest": { label: "SA.Smallest", size: 28 },
  "small": { label: "SA.Small", size: 40 },
  "medium": { label: "SA.Medium", size: 56 },
  "large": { label: "SA.Large", size: 64 },
  "largest": { label: "SA.Largest", size: 72 },
};
